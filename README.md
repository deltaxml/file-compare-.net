# File Compare sample
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DocBook-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the exe files.*

*For example `DeltaXML-DocBook-Compare-5_2_2_n/samples/sample-name`.*

---

## 1. Running the sample from the Command line

It is possible to compare two DocBook files using the command line tool from sample directory as follows.

    ../../bin/deltaxml-docbook.exe compare docbook-article-version-1.xml docbook-article-version-2.xml docbook-article-revision.xml indent=yes
    
The *docbook-article-revision.xml* file contains the result, which is indented due to setting the *indent* parameter to *yes*. 

Other parameters are available as discussed in the [User Guide](https://docs.deltaxml.com/docbook-compare/latest/user-guide) and summarised by the following command.

    ../../bin/deltaxml-docbook.exe describe